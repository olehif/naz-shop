class OrdersController < ApplicationController
  def create
    Order.create(order_params)
    redirect_back(fallback_location: root_path)
  end

  private

  def order_params
    params.require(:order).permit(:quantity, :product_id, :comment, :name, :phone)
  end
end
