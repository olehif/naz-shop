Rails.application.routes.draw do
  root 'products#index'
  resources :products, only: [:show, :index]
  resources :orders, only: [:create]
end
