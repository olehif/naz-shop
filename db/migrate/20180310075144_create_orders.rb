class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :quantity
      t.references :product, foreign_key: true
      t.text :comment
      t.string :name
      t.string :phone

      t.timestamps
    end
  end
end
